<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth', 'UsersController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/tasks', 'TasksController@index');
    Route::post('/tasks', 'TasksController@store');
    Route::post('/tasks/{task}/comments', 'CommentsController@store');
});
