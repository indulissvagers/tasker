<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\User;
use App\Models\Task;

/**
 * Interface TaskRepository.
 */
interface TaskRepository extends RepositoryInterface
{
    /**
     * Store task created by the User.
     *
     * @param User  $user
     * @param array $data
     *
     * @return Task
     */
    public function createUserTask(User $user, array $data): Task;
}
