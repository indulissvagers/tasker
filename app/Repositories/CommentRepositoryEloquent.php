<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Task;
use App\Models\User;
use App\Models\Comment;

/**
 * Class CommentRepositoryEloquent.
 */
class CommentRepositoryEloquent extends BaseRepository implements CommentRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Comment::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Presenter.
     */
    public function presenter()
    {
        return 'App\\Presenters\\CommentPresenter';
    }

    /**
     * Create comment for the given Task.
     *
     * @param Task  $task
     * @param User  $user
     * @param array $data
     *
     * @return Comment
     */
    public function createTaskComment(Task $task, User $user, array $data): Comment
    {
        $data['user_id'] = $user->id;

        return $task->comments()->create($data);
    }
}
