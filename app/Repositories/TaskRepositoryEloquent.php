<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\User;
use App\Models\Task;
use App\Criteria\ByAssigneeIdCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class TaskRepositoryEloquent.
 */
class TaskRepositoryEloquent extends BaseRepository implements TaskRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title' => 'like',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Task::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(ByAssigneeIdCriteria::class));
    }

    /**
     * Presenter.
     */
    public function presenter()
    {
        return 'App\\Presenters\\TaskPresenter';
    }

    /**
     * Store task created by the User.
     *
     * @param User  $user
     * @param array $data
     *
     * @return Task
     */
    public function createUserTask(User $user, array $data): Task
    {
        return $user->tasks()->create($data);
    }
}
