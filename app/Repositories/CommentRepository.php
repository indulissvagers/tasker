<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Task;
use App\Models\User;
use App\Models\Comment;

/**
 * Interface CommentRepository.
 */
interface CommentRepository extends RepositoryInterface
{
    /**
     * Create comment for the given Task.
     *
     * @param Task  $task
     * @param User  $author
     * @param array $data
     *
     * @return Comment
     */
    public function createTaskComment(Task $task, User $author, array $data): Comment;
}
