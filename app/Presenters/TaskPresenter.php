<?php

namespace App\Presenters;

use App\Transformers\TaskTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TaskPresenter.
 */
class TaskPresenter extends FractalPresenter
{
    /**
     * @var string
     */
    protected $resourceKeyItem = 'task';

    /**
     * @var string
     */
    protected $resourceKeyCollection = 'tasks';

    /**
     * Transformer.
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TaskTransformer();
    }
}
