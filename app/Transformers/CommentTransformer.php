<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Comment;

/**
 * Class CommentTransformer.
 */
class CommentTransformer extends TransformerAbstract
{
    /**
     * Default includes.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'author',
    ];

    /**
     * Transform the Comment entity.
     *
     * @param \App\Models\Comment $model
     *
     * @return array
     */
    public function transform(Comment $model)
    {
        return [
            'id' => (int) $model->id,
            'message' => $model->message,
            'created_at' => $model->created_at,
        ];
    }

    /**
     * Includes author.
     *
     * @param Comment $comment
     */
    public function includeAuthor(Comment $comment)
    {
        $user = $comment->user;

        if ($user) {
            return $this->item($user, new UserTransformer(), 'user');
        }
    }
}
