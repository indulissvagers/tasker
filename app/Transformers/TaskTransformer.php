<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Task;

/**
 * Class TaskTransformer.
 */
class TaskTransformer extends TransformerAbstract
{
    /**
     * Default includes.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'assignee',
        'author',
        'comments',
    ];

    /**
     * Transform the Task entity.
     *
     * @param \App\Models\Task $model
     *
     * @return array
     */
    public function transform(Task $model)
    {
        return [
            'id' => (int) $model->id,
            'title' => $model->title,
            'description' => $model->description,
            'created_at' => $model->created_at,
        ];
    }

    /**
     * Icludes owner of the Task.
     *
     * @param Task $task
     */
    public function includeAuthor(Task $task)
    {
        $user = $task->user;

        if ($user) {
            return $this->item($user, new UserTransformer(), 'user');
        }
    }

    /**
     * Includes task assignee.
     *
     * @param Task $task
     */
    public function includeAssignee(Task $task)
    {
        $assignee = $task->assignee;

        if ($assignee) {
            return $this->item($assignee, new UserTransformer(), 'user');
        }
    }

    /**
     * Includes task comments.
     *
     * @param Task $task
     */
    public function includeComments(Task $task)
    {
        $comments = $task->comments;

        if ($comments) {
            return $this->collection($comments, new CommentTransformer(), 'comments');
        }
    }
}
