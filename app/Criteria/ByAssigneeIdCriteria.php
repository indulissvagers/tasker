<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class ByAssigneeIdCriteria.
 */
class ByAssigneeIdCriteria implements CriteriaInterface
{
    /**
     * HTTP request.
     *
     * @var Request
     */
    protected $request;

    /**
     * ByAssigneeIdCriteria Constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('assignee_id')) {
            $model = $model->where('assignee_id', $this->request->get('assignee_id'));
        }

        return $model;
    }
}
