<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Initiate User token.
     *
     * @param Request $request
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid credentials',
                ], 400);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Could not create token',
            ], 500);
        }

        return response()->json([
            'data' => [
                'type' => 'token',
                'attributes' => [
                    'access_token' => $token,
                    'expires' => Carbon::now()->addMinutes(60),
                ],
            ],
        ], 200);
    }
}
