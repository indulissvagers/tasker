<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentCreateRequest;
use App\Models\Task;
use App\Repositories\CommentRepository;
use Auth;

class CommentsController extends Controller
{
    /**
     * @var CommentRepository
     */
    protected $repository;

    /**
     * CommentsController constructor.
     *
     * @param CommentRepository $repository
     */
    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates comments.
     *
     * @param CommentCreateRequest $request
     * @param Task                 $task
     */
    public function store(CommentCreateRequest $request, Task $task)
    {
        $comment = $this->repository->createTaskComment($task, Auth::user(), $request->all());

        return response()->json($this->repository->find($comment->id), 201);
    }
}
