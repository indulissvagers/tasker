<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TaskCreateRequest;
use App\Repositories\TaskRepository;
use Auth;

/**
 * Class TasksController.
 */
class TasksController extends Controller
{
    /**
     * @var TaskRepository
     */
    protected $repository;

    /**
     * TasksController constructor.
     *
     * @param TaskRepository $repository
     */
    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = $this->repository->paginate(15, ['*']);

        return response()->json($tasks, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TaskCreateRequest $request)
    {
        $task = $this->repository->createUserTask(Auth::user(), $request->all());

        return response()->json($this->repository->find($task->id), 201);
    }
}
