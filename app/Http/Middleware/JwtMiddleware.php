<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                throw new UnauthorizedHttpException('challenge', 'Token is Invalid', null, 401);
            } elseif ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                throw new UnauthorizedHttpException('challenge', 'Token is Expired', null, 401);
            } else {
                throw new UnauthorizedHttpException('challenge', 'Authorization Token not found', null, 401);
            }
        }

        return $next($request);
    }
}
