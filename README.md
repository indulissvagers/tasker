# Tasker

This is a sample Laravel app

### Deployment

Install dependencies

```
composer install --no-dev --optimize-autoloader
```

Migrate database
```
php artisan migrate
```

Seed if necessary
```
php artisan db:seed
```

Test
```
./vendor/bin/phpunit
```

### Comments

No user registration endpoints are created, as not specified.
Listing and posting endpoints are under guard as they would be in real case.
