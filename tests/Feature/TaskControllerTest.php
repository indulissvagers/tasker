<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Support\Facades\Artisan;
use App\Models\User;
use Tests\CreatesApplication;
use JWTAuth;
use App\Models\Task;
use App\Models\Comment;

class TaskControllerTest extends TestCase
{
    use CreatesApplication, DatabaseMigrations, RefreshDatabase;

    /**
     * Logged in user.
     *
     * @var User
     */
    private $user;

    /**
     * Access token.
     *
     * @var string
     */
    private $token;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed');

        $this->user = factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('secret'),
        ]);

        $this->token = JWTAuth::fromUser($this->user);
    }

    /** @test */
    public function testUnauthorizedTaskListing()
    {
        $task = Task::first();
        $comment = factory(Comment::class)->raw();

        $this->get('/tasks', $comment)
            ->assertStatus(401);
    }

    /** @test */
    public function testTaskListing()
    {
        $response = $this->get('/tasks', ['Authorization' => "Bearer $this->token"]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'included',
                'meta' => [
                    'pagination' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages',
                    ],
                ],
                'links' => [
                    'self',
                    'first',
                    'last',
                ],
            ]);
    }

    /** @test */
    public function testTaskListingByAssignee()
    {
        $assignee_id = 2;
        $response = $this->get('/tasks?assignee_id='.$assignee_id, ['Authorization' => "Bearer $this->token"]);

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $count = count($data['data']);
        $total = Task::where('assignee_id', $assignee_id)->count();
        $expected = ($total > 15) ? 15 : $total;

        $this->assertEquals($count, $expected);
    }

    /** @test */
    public function testTaskListingByTitle()
    {
        $task = Task::first();
        $response = $this->get('/tasks?title='.$task->title, ['Authorization' => "Bearer $this->token"]);

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $count = count($data['data']);
        $total = Task::where('title', 'like', '%'.$task->title.'%')->count();
        $expected = ($total > 15) ? 15 : $total;

        $this->assertEquals($count, $expected);
        $this->assertGreaterThanOrEqual(1, $total);
    }

    /** @test */
    public function testUnauthorizedTaskCreation()
    {
        $task = factory(Task::class)->raw();

        $this->post('/tasks', $task)
            ->assertStatus(401);
    }

    /** @test */
    public function testTaskCreation()
    {
        $task = factory(Task::class)->raw();
        $response = $this->post('/tasks', $task, ['Authorization' => "Bearer $this->token"]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'task',
                    'attributes' => [
                        'title' => $task['title'],
                        'description' => $task['description'],
                    ],
                    'relationships' => [
                        'assignee' => [
                            'data' => [
                                'type' => 'user',
                                'id' => $task['assignee_id'],
                            ],
                        ],
                        'author' => [
                            'data' => [
                                'type' => 'user',
                                'id' => $this->user->id,
                            ],
                        ],
                    ],
                ],
            ]);
    }

    /** @test */
    public function testTaskCreationFailure()
    {
        $task = [
            'title' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
            'description' => 't',
        ];

        $this->post('/tasks', $task, ['Authorization' => "Bearer $this->token"])
            ->assertStatus(422)
            ->assertJson([
                'data' => [
                    'status' => 'error',
                ],
            ])
            ->assertJsonStructure([
                'data' => [
                    'errors' => [
                        'title',
                        'description',
                        'assignee_id',
                    ],
                ],
            ]);
    }

    /** @test */
    public function testUnauthorizedTaskCommentCreation()
    {
        $task = Task::first();
        $comment = factory(Comment::class)->raw();

        $this->post('/tasks/'.$task->id.'/comments', $comment)
            ->assertStatus(401);
    }

    /** @test */
    public function testTaskCommentCreation()
    {
        $task = Task::first();
        $comment = factory(Comment::class)->raw();
        $response = $this->post('/tasks/'.$task->id.'/comments', $comment, ['Authorization' => "Bearer $this->token"]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'comment',
                    'attributes' => [
                        'message' => $comment['message'],
                    ],
                    'relationships' => [
                        'author' => [
                            'data' => [
                                'type' => 'user',
                                'id' => $this->user->id,
                            ],
                        ],
                    ],
                ],
            ]);
    }

    /** @test */
    public function testTaskCommentCreationFailure()
    {
        $task = Task::first();
        $comment = [];

        $this->post('/tasks/'.$task->id.'/comments', $comment, ['Authorization' => "Bearer $this->token"])
            ->assertStatus(422)
            ->assertJson([
                'data' => [
                    'status' => 'error',
                ],
            ])
            ->assertJsonStructure([
                'data' => [
                    'errors' => [
                        'message',
                    ],
                ],
            ]);
    }
}
