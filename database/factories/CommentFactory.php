<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'message' => $faker->realText(200),
        'user_id' => $faker->numberBetween(1, 5),
        'task_id' => $faker->numberBetween(1, 5),
    ];
});
