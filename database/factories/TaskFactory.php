<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Task::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->realText(200),
        'assignee_id' => $faker->numberBetween(1, 5),
    ];
});
