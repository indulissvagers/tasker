<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(App\Models\User::class, 5)->create()->each(function ($user) {
            $user->tasks()->save(factory(App\Models\Task::class)->make());
        });
    }
}
